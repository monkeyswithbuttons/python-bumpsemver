python-bumpsemver
=================

.. image:: https://gitlab.com/monkeyswithbuttons/python-bumpsemver/badges/master/pipeline.svg
    :alt: pipeline

.. image:: https://gitlab.com/monkeyswithbuttons/python-bumpsemver/badges/master/coverage.svg
    :alt: coverage

Check the docs at `<https://monkeyswithbuttons.gitlab.io/python-bumpsemver>`_.
