.. bumpsemver documentation master file, created by
   sphinx-quickstart on Thu Nov 23 10:26:04 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bumpsemver's documentation!
======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


.. automodule:: bumpsemver
   :members: major, minor, patch


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
