# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py

from setuptools import setup, find_packages


with open('README.rst') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='bumpsemver',
    version='0.1.0',
    description='Bumps a semver version.',
    long_description=readme,
    author='Greg Trahair',
    author_email='greg@monkeyswithbuttons.com',
    url='https://github.com/monkeyswithbuttons/bumpsemver',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)

