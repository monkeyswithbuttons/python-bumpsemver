# -*- coding: utf-8 -*-
def patch(version):
    """Take the version and increment the patch level.

    >>> patch('0.1.0')
    '0.1.1'

    """
    major, minor, patch = version.split('.')
    int(major)
    int(minor)
    patch = int(patch) + 1

    return '{0}.{1}.{2}'.format(major, minor, patch)


def minor(version):
    """Take the version and increment the minor level.

    >>> minor('0.1.0')
    '0.2.0'

    """
    major, minor, patch = version.split('.')
    int(major)
    minor = int(minor) + 1
    patch = 0

    return '{0}.{1}.{2}'.format(major, minor, patch)


def major(version):
    """Take the version and increment the major level.

    >>> major('0.1.0')
    '1.0.0'

    """
    major, minor, patch = version.split('.')
    major = int(major) + 1
    minor = 0
    patch = 0

    return '{0}.{1}.{2}'.format(major, minor, patch)
