init:
	pip install -r requirements.txt

test:
	python setup.py test

watch:
	watchman-make -p '**/*.py' -t test

htmldocs:
	$(MAKE) -C docs html

docker-shell:
	docker run -v $(CURDIR):/usr/src/bumpsemver -w /usr/src/bumpsemver --rm -ti python:3-alpine sh
