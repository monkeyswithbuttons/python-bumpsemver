# -*- coding: utf-8 -*-
import unittest
import bumpsemver


class BumpVersionTest(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_patch_increment(self):
        v = bumpsemver.patch('0.1.0')
        self.assertEqual('0.1.1', v)

    def test_minor_increment(self):
        v = bumpsemver.minor('0.1.0')
        self.assertEqual('0.2.0', v)

    def test_major_increment(self):
        v = bumpsemver.major('0.1.0')
        self.assertEqual('1.0.0', v)


if __name__ == '__main__':
    unittest.main()
